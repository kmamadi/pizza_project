Rails.application.routes.draw do
  devise_for :users

  root 'boards#index'

  resources :boards
  get "/webhooks/receive", to: "webhooks#complete"
  post "/webhooks/receive", to: "webhooks#receive"
end
