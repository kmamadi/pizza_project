require 'rails_helper'

RSpec.describe Card, type: :model do
  describe 'create card' do
    it 'will store card details' do
      card_count = Card.all.count
      Card.create!(name: 'First testing ticket', list_id: '1')
      Card.create!(name: 'Second testing ticket', list_id: '1')
      expect(Card.all.count).to eq (card_count + 2)
      expect(Card.last.list_id).to eq '1'
      expect(Card.last.name).to eq 'Second testing ticket'
    end
  end
end