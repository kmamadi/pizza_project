class CreateCard < ActiveRecord::Migration[5.2]
  def up
    create_table :cards do |t|
      t.string :name
      t.string :list_id
      t.timestamps
    end
  end
end