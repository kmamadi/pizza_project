# README

This README is for a Rails App linked to a Trello board where the following can be done

- View all lists
- Create new cards

To run this app, run the following

- bundle install
- rake db:migrate
- rails s

The app can be found on localhost:3000 once running locally or alternatively can be found here 
https://49ca-160-19-36-2.ngrok.io

The trello board linked is found here https://trello.com/b/horwnrwz