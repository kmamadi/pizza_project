class BoardsController < ApplicationController

  def index
    board = Trello::Member.find('mamadikabelo').boards.first
    @board_name = board.name
    @lists = board.lists
    @cards = board.cards
    list_id = @lists.first.id
    # webhook_details = create_webhook(list_id)
    # Trello::Webhook.create(trello_id: webhook_details['id'],
    #                        active: true,
    #                        callback_url: webhook_details['callbackURL'],
    #                        board_id: board.id,
    #                        trello_model_type: 'Board')
  end

  def new
    @card = Card.new
  end

  def create
    member = Trello::Member.find('mamadikabelo')
    list_id = member.boards.first.lists.first.id
    card = Card.create!(list_id: list_id, name: card_params[:name])

    card = Trello::Card.create(
      name: card.name,
      list_id: card.list_id,
      member_ids: [ member.id ]
    )
    redirect_to root_path
  end

  def create_webhook(trello_board_id)
    member = Trello::Member.find('mamadikabelo')
    list = member.boards.first.lists.first
    list_id = list.id
    url = 'https://49ca-160-19-36-2.ngrok.io'
    developer_public_key = '2351e84812a80d260e9c7893fedd7864'
    member_token = 'b14ed13ffc35d84ca18d60e6b505db23d818173d0064ecfeb0f35b9da1515141'
    # HTTParty.post("https://api.trello.com/1/tokens/#{member_token}/webhooks/?key=#{developer_public_key}",
    #               query: { callbackURL: url, idModel: list_id },
    #               headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    #               timeout: 35)
  end

  private

  def card_params
    params.require(:card).permit(:id, :name, :list_id)
  end

end
